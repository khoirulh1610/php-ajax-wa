<?php include('conn.php'); ?>
<style>
    
.table {
  border-collapse: collapse;
}
</style>
<html>
<head>
    <title>WA</title>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>
    <h2>Pesan Masuk</h2>
    <table class="table" border="1" width="100%">
        <thead>
            <th>Chatid</th>
            <th>from</th>
            <th>Message</th>
            <th>Type</th>
            <th>Created At</th>            
        </thead>
        <tbody id="tbdata">
            <?php include('inbox.data.php'); ?>
        </tbody>
    </table>
    <h2>Pesan Keluar</h2>
    <table class="table" border="1" width="100%">
        <thead>
            <th>Chatid</th>
            <th>from</th>
            <th>Message</th>
            <th>Type</th>
            <th>Created At</th>            
        </thead>
        <tbody id="tbdata2">
            <?php include('outbox.data.php'); ?>
        </tbody>
    </table>
    <script>
        var tmp = "";
        setInterval(function(){
            $.ajax({
                url : "http://<?=$_SERVER['HTTP_HOST'].'/phpwa/inbox.data.php';?>",
                type : 'get',
                success : function(data){
                    if(tmp != data){
                        $('#tbdata').html(data);
                        tmp=data;
                    }
                    
                }
            });    
        }, 3000);

        var tmp2 = "";
        setInterval(function(){
            $.ajax({
                url : "http://<?=$_SERVER['HTTP_HOST'].'/phpwa/outbox.data.php';?>",
                type : 'get',
                success : function(data){
                    if(tmp2 != data){
                        $('#tbdata2').html(data);
                        tmp2=data;
                    }
                    
                }
            });    
        }, 3000);
        
    </script>
</body>
</html> 