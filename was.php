<?php include('conn.php'); ?>
<style>
    
.table {
  border-collapse: collapse;
}
</style>
<html>
<head>
    <title>List WA</title>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>
    <h2>List Whatsapp</h2>
    <table class="table" border="1" width="100%">
        <thead>
            <th>NO.</th>
            <th>Session</th>
            <th>Server</th>
            <th>Name</th>
            <th>Batetry</th>
            <th>Status</th>
        </thead>
        <tbody id="tbdata">
            <?php include('was.data.php'); ?>
        </tbody>
    </table>
    <script>
        var tmp = "";
        setInterval(function(){
            $.ajax({
                url : "http://<?=$_SERVER['HTTP_HOST'].'/phpwa/was.data.php';?>",
                type : 'get',
                success : function(data){
                    if(tmp != data){
                        $('#tbdata').html(data);
                        tmp=data;
                    }
                    
                }
            });    
        }, 3000);
        
    </script>
</body>
</html>