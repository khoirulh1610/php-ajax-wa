const sulla = require('venom-bot');
const fs = require('fs');
const mime = require('mime-types');
var mysql = require('mysql');
require('dotenv').config();
var userid = 0;
var was_id = "0";
var server_id = "";
var con = mysql.createConnection({
    host: process.env.mysql_host,
    user: process.env.mysql_user,
    password: process.env.mysql_password,
    database: process.env.mysql_db,
    port : process.env.mysql_port
  });
con.connect(function(err) {
    if (err) throw err;  
});


function exportQR(qrCode, sess) {
    con.query("update was set barcode='"+ qrCode + "' where session='"+sess+"'");    
}

// console.log(process);

module.exports = {
    StartSession: async(sess)=>{
        con.query("update was set pid='" + process.pid + "',status=1,updated_at=now() where session='"+sess+"'");
        sulla.create(sess, (base64Qr, asciiQR) => {        
            exportQR(base64Qr, sess);
        },(statusFind) => {
            console.log(statusFind);
          },{
            headless: true,
            useChrome:false,
	        browserArgs: ['--no-sandbox'],
            refreshQR: 15000
        }).then(async(client)=>{
            
            //----------------
            client.onStateChange((state) => {
                console.log(state);
                con.query("update was set state='"+state+"' where session='"+sess+"'");            
                const conflits = [
                  sulla.SocketState.CONFLICT,
                  sulla.SocketState.UNPAIRED,
                  sulla.SocketState.UNLAUNCHED,
                ];
                if (conflits.includes(state)) {
                  client.useHere();
                }
                if(state=='UNPAIRED' || state == 'UNPAIRED_IDLE'){                    
                    con.query("update was set status=0,contact=null,battery=0,pushname=null,connected=0,barcode=null where session='"+sess+"'");        
                }
            });


            // ------------------------//
            setUserSession(sess);
            ImpContacts(client);            
            
            getStatus(client,sess);
            setInterval(function(){
                getStatus(client,sess);
            }, 60000);
            
            
            
            client.onMessage(async(message) => {
                // console.log(message);
                fs.writeFile('message.json',JSON.stringify(message),'utf8',function(err){
                    console.log(err);
                });
                Inbox(message,sess,client);
                //download file / media                
                await client.sendSeen(message.id);
                //cek prifile
                const profile = await client.getProfilePicFromServer(message.sender.id);
                console.log(profile);
            })
             
            //----------------------------
            //tamhabkan 7 hour jika locale bukan indonesia
            setInterval(() => {
                Outbox(client,sess);
            }, 2000);

            //   ----------------------------------//
            client.onAck(ack => {
                fs.writeFile('ack.json',JSON.stringify(ack),'utf8',function(err){
                    con.query("select * from chats where messageid='"+ack.id._serialized+"'",function(err,rows){
                        if(rows.length > 0){
                            con.query("update chats set ack="+ack.ack+" where messageid='"+ack.id._serialized+"'");
                        }else{
                            con.query("insert into chats(userid,was_id,csid,chatid,`from`,`to`,message,sent_at,created_at,ack,session,prosesed,messageid)values("+userid+","+was_id+","+userid+",'"+ack.id.remote+"','"+ack.from+"','"+ack.to+"','"+ack.body+"',now(),now(),"+ack.ack+",'"+sess+"',1,'"+ack.id._serialized+"')");
                        }
                    });
                });  
                console.log(ack);
            });

            
        });        
    }
}

async function Outbox(client,sess){
    con.query("select * from chats where chat_type='out' and prosesed=0 and sent_at<=date_add(now(),INTERVAL 0 HOUR) and session='"+sess+"' limit 0,1", function(err, result) {
        if (err) {
            console.log(err);      
        }
        
            if(result.length > 0){
                
                    let hp = result[0].chatid.replace(/^0/,'62');
                    let target = (hp.indexOf('@c.us')>1)?hp:hp+'@c.us';
                    if(result[0].type=='chat'){                                                            
                            target = (result[0].is_group==='N') ? target : result[0].chatid;
                            msg = result[0].message;
                            SendChat(client,target,msg,result[0].id);                            
                    }else if(result[0].type=='image'){
                        let file_path = result[0].path;                            
                        // await client.sendImageAsSticker(hp, 'd:\\usericon.png');
                    }                        
                    
                
            }                    
        
    });
}

async function Inbox(msg,sess,client) {
    let id = msg.id;
    let isi = msg.body.replace("'"," ");
    let type = msg.type;
    let tm = msg.t;
    let frm = msg.from;
    let to = msg.to;    
    let ack = msg.ack;    
    let isForwarded = (msg.isForwarded) ?1:0;
    let sender_name = msg.sender.name;
    let isGroupMsg = (msg.isGroupMsg)?1:0;    
    let isOnline = (msg.isOnline)?1:0;
    let lastSeen = msg.lastSeen;
    let chatId = msg.chatId;
    let pushname = msg.sender.pushname ? msg.sender.pushname : msg.sender.verifiedName;
    let pp_pict = msg.sender.profilePicThumbObj.eurl;
    let path_media = '';
    if (msg.isMedia || msg.document=='document') {        
        const buffer = await client.decryptFile(msg);                    
        const fileName = msg.id + `.${mime.extension(msg.mimetype)}`;
        fs.writeFile( process.env.path_media + '/' + fileName, buffer, function (err) {
          if(err)
            console.log(err);
            path_media = fileName;
            console.log(path_media);
        });
        setTimeout(() => {
            
        }, 5000);
    }
    con.query("insert into chats(userid,was_id,session,chatid,`from`,`to`,message,chat_name,messageid,isForwarded,type,is_group,name,pushname,timestamp,ack,online,lastSeen,prosesed,pp_pict,created_at,chat_type,path_media)values"+
    "("+userid+","+was_id+",'"+sess+"','"+chatId+"','"+msg.sender.id+"','"+to+"','"+isi+"','"+msg.chat.name+"','"+id+"','"+isForwarded+"','"+type+"','"+isGroupMsg+"','"+sender_name+"','"+pushname+"','"+tm+"','"+ack+"','"+isOnline+"',"+lastSeen+",0,'"+pp_pict+"',now(),'in','"+path_media+"')");
    // con.query("update chats a,was b set a.userid=b.userid where a.`session`=b.`session` and a.userid=0");
    // Bot Autoresponse
    con.query("select * from wabots where perintah='" + isi + "' and session='"+sess+"'",function(err,result,fields) {
        if(err){
            console.log(err);
        }else{
            if(result.length>0){
                var d = new Date();
                var tgl = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();
                var tm = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                var msg = result[0].response;
                msg = msg.replace('<<date>>',tgl);
                msg = msg.replace('<<time>>',tm);
                msg = msg.replace('<<ln>>','\r\n');
                con.query("insert into waoutboxs(session,chatid,message)values('"+sess+"','"+frm+"','"+msg +"')");
            }   
        }
    });
    SaveContact(msg.sender.id,msg.sender.name,pushname,pp_pict);
    
}

async function SaveContact(contact,name,pushname,img=''){
    con.query("select * from contacts where contact='"+ contact + "' and type='whatsapp' and was_id=" + was_id + " and userid="+userid,function(err,rows,fl){
        if(err){
            console.log(err);
        }else{
            if(rows.length>0){
                con.query("update contacts set link_img_profile='"+img+"',name='"+name.replace(/'/g, "\\'")+"',pushname='"+pushname.replace(/'/g, "\\'")+"' where contact='"+contact+"' and was_id=" + was_id + " and userid="+userid,function(err,row,fl) {
                    if(err)
                        console.log(err);
                });   
            }else{
                con.query("insert ignore into contacts(userid,was_id,contact,name,pushname,type,created_at)values("+userid+","+was_id+",'"+contact+"','"+name.replace(/'/g, "\\'")+"','"+pushname.replace(/'/g, "\\'")+"','whatsapp',now())",function(err,row,fl) {
                    if(err)
                        console.log(err);
                });                
            }
        }
    });
 }

 
    
function setUserSession(sess) {
    let uid=0;
    con.query("select * from was where session='"+sess+"'",function(err,rows,fields){
        if(err){
            console.log(err);
        }else{
            if(rows.length>0){
                userid = rows[0].userid;
                was_id = rows[0].id;
                server_id = rows[0].contact;
            }         
            console.log("User id : " + userid + " was_id : " + was_id + " Server ID : " + server_id);       
        }
    });
    return uid;
}

async function ImpContacts(client){
    let contacts = await client.getAllContacts();
    fs.writeFile('contacts.json',JSON.stringify(contacts),'utf8',function(err){
        for (let i = 0; i < contacts.length; i++) {
            const item = contacts[i];
            // console.log(item);
            if(item.isMyContact){
                // SaveContact(item.id._serialized,item.formattedName,item.name);
            }
        }
    });
}

async function SendChat(client,target,msg,id=0) {
    let msgid = await client.sendMessageToId(target,msg); 
    console.log(msgid);
    con.query("update chats set prosesed=1,messageid='"+msgid+"' where id="+ id);                                         
}

async function getStatus(client,sess) {
    let wa = await client.getHostDevice();      
    let state = await client.getConnectionState();            
    console.log('State :' + state);                  
    console.log(wa);
    con.query("update was set contact='"+wa.me.user+"',battery='"+wa.battery+"',pushname='"+wa.pushname+"',state='"+state+"',status=1,pid='"+process.pid+"',connected='"+ (state=='CONNECTED' ? 1: 0) +"',barcode=null where session='"+sess+"'");    
}